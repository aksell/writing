%!TEX root = ../Thesis.tex
\chapter{Discussion}
\section{Is the implemented system useful?}
Personal time tracking is typically done via phone apps or on a laptop.
Tracking time with a smartwatch has some advantages over a traditional phone or desktop application.
Smartwatches with hardware buttons are very fast to interact with if one only needs to input a few bits of information.
They are very accessible, being strapped to one's body for most of the day.
They are easier to set up for minimal distractions, so one reduces the risk of getting tempted by
notifications on one's phone when opening it to update one's time tracking system.

However, time tracking with a watch is also limited. Inputting more than a few bits of information is
not easily done without voice-to-text support, which most low-cost programmable smartwatches lack.
Because of this one is reliant on pre-configuring a watch's time tracking app with categories one
is interested in tracking, rather than entering time tracking information free form as it happens.
This reduces both the flexibility and the time needed to track time.
Because of their availability, time tracking with smartwatches can allow one to rely on
inputting changes in activities as they happen instead of entering them in batch after the fact.
This could lead to more accurate data, as the data is less likely to be skewed by recollection errors.

The implemented time tracking app has no utilities for editing time tracking history. Allowing one
to edit history to fix mistakes one makes in tracking time could lead to more accurate data.
However, this is likely easier to implement in a phone app, as keeping a list of old entries that might
be synced with a phone and updating them after the fact is likely tricky to implement. Hopefully, the
need to edit entries will be rare after one has used the system for a while editing entries should be a rare occurrence due to the occasional slip in tracking accuracy.
Because editability is likely not needed often, it is okay to have it implemented in the phone app instead of the watch app.

One possible strength of time tracking with smartwatches is that it could be fast enough to develop something akin to muscle memory for tracking time because of speedy interactions.
If one gets to such a place, it could be that sticking to time tracking long-term is easier, as it is no longer perceived as an effortful task one puts off. Instead, it is an almost automatic act.

Suppose one is interested in knowing where one's time goes. In that case, one could spend less time tracking by also
using automatic tracking systems in a context where automatic tracking is likely more accurate than manual tracking. Computer time or other time spent on digital devices are good candidates for automatic tracking. Exercise is a good candidate for tracking with motion data, as many smartwatches support recognizing active time out of the box.

One could imagine an idealized setup where the strengths of automatic time tracking of digital activities
are used alongside a smartwatch based tracking system:
\begin{itemize}
    \item rescutime - Time on specific apps and websites. Total laptop time
    \item ttrack on a smartwatch - Track nondigital activities in custom categories using the quick interface.
\end{itemize}
With this one could reduce the category count in a smartwatch tracking app. Time on digital devices would simply
be bucketed in one category to be handled by other services.

But there are time tracking metrics on digital devices that are hard to capture with automatic systems.
This could be:
\begin{itemize}
    \item Time spent working towards longer-term goals vs. time spent responding to and doing work as it pops up
    \item Self-perceived work intensity. Track how much time one feels like one is focusing well or doing deep work.
\end{itemize}
Here a smartwatch app could be useful because of its quick interaction time and lack of distractions.
And the fact that the distinction between these categories is hard to categorize automatically, being largely based
on personal judgment.

Automatic category recognition using motion data is an interesting idea, with a lot of active research.
But it was not implemented in a way that was useful for daily time tracking in this thesis.

A smarter system that integrates with one's to-do lists or issue boards could be a way to circumvent the
limited input speed one has in a smartwatch. By allowing the relevant options of time tracking categories,
such as projects at work or personal to-dos, to be on the watch through an integration with one's task tracking
system instead of manually maintaining a set of time tracking categories for the watch.

The implemented app for tracking time via a smartwatch app is faster for tracking nondigital activity than comparable
phone or desktop applications.
The visualizations created in Metabase provide visually appealing dashboards for how one is spending time.

\section{Is the Bangle.js the right smartwatch?}
Out of the three watches reviewed in chapter \ref{cha:review} (the PineTime, pebble-time, and the Bangle.js), the Bangle.js
was chosen as the watch platform to implement the time tracking tool for.
The three reviewed watches all have in common that they are easily programmable and reasonably priced.
The three watches are also not very widely used.
The Bangle.js had a crowdfunding campaign where 1300 watches were sold \footnote{\url{https://www.kickstarter.com/projects/gfw/banglejs-the-hackable-smart-watch}}, the watch has also been for sale for
roughly a year, but there are likely not more than a few thousand watches in use. The average
Bangle.js user is likely to be someone interested in tinkering with hardware.
Because of this, the implemented time tracking system will likely not see widespread usage.
Android wear was not seriously considered for this thesis due to lack of experience with
java development and the price point of most smartwatches running android wear.
The Bangle.js was a nice platform for developing a proof of concept app that is useable for
users with some tech competence. If broader adoption of the app was a goal, an easily installable android wear application would likely have been a better approach.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Future work}
Suggestions for ways the time tracking app could be improved.
\subsection{Nudging behavior}
The current system attempts to make it fast and easy to record how one is spending time in a day.
In the hopes that this makes the data more accurate and that it makes the user more likely to stick with tracking time long
term.
After recording time tracking data, it is up to the user to create custom Metabase dashboards with metrics that
motivate behavior change, if that is why one is tracking time.
If one does not set up motivating visualizations, or one does not view them regularly one could argue that tracking time has little long-term value.
Tracking time and displaying it is not an end in itself. Tracking time (when not doing it for an employer) is
only useful if it can be an enabler for some other goals one has.
To give more value
the watch app could be set up to try and give nudges when one strays far from user-defined goals for how they would like to spend time.
This could be paired with motivating suggestions for other activities when one passes the limit one has set for an activity.

This could also be implemented as a simple notification when one passes a set time quota for a category, in case the user themselves were not aware of how much time they had spent.

\subsection{Improved on watch visualization}
Accessing Metabase on one's laptop is cumbersome if one is not at one's laptop. This makes it
ill-suited for quick checks. One solution to this is to host Metabase on a public domain, with a cloud hosting service
instead of one's local machine. Using the Metabase user system to ensure the data was safe even when the service was open to the
internet. This would allow accessing the visualizations via any browser, for example, using one's phone.
A second alternative to get some of the motivational effects of visually appealing visualizations would be implementing some
basic visualizations in the watch app. The visualizations could display how one spends time, compared to an average day, or
relative to user-defined goals per time tracking category.
Many smartwatches have motivating visualizations for helping one reach fitness or activity goals.
For example, the apple watch uses three rings to visualize one's daily goal for hours of the day
spent standing, active time, and active calories. See figure \ref{fig:apple-rings}.
Something similar could be implemented for user-defined time tracking categories to gamify
spending time following the user's self-defined goals.

\begin{figure}[ht]
    \centering
    \includegraphics[width=0.5\textwidth]{img/apple-watch-rings.png}
    \caption{The Apple watch's "close the rings" activity visualization}
    \label{fig:apple-rings}
\end{figure}

\subsection{Data sync robustness}
Syncing data to Gadgetbridge is not very robust. Entries are deleted from the watch after being successfully sent from the watch,
without any confirmation from Gadgetbridge that the relevant entries were parsed and committed successfully.
A message back to the watch indicating how many time tracking entries were committed to the database is a low effort way of improving this.

\subsection{Stochastic category check algorithm}
The current system records category checks. These checks are random snapshots of what one is doing
and are meant to measure one's time tracking accuracy, to give an impression of one's time tracking data quality.
The checks in themselves will occasionally increase tracking accuracy by "catching" the user in the wrong category
and prompting them to correct their currently selected category.

The check interval length is a balance between how often we are willing to be disturbed/pinged
and the accuracy of category check data, as well as the overall tracking accuracy.
The implemented category check system uses a fixed average period between checks.
An algorithm for easing off on or increasing check frequency based on check performance could increase the tracking accuracy and reduce the total annoyance from category checks by reducing checks that do not cause the user to update their active category.

\subsection{Automatic category recognition}
This report did not try many approaches for recognizing categories using the watch's sensors.
Automatic category recognition with motion data probably has more potential and should be explored further. There is a lot of active research into
activity recognition using motion data from wearable devices. The novelty of the approach detailed in this thesis would be
that one could run and calibrate an activity recognition model alongside a manual tracking system.
This could be advantageous by blurring the distinction between periods where one gathers and tags motion data
and the normal use of the system. In essence, the user is always tagging their motion data by using the
manual time tracking system.
Because of the system's hybrid manual and "automatic" approach, the automatic category recognition model would only have to be accurate enough to recognize that you are not
in your current category so that it can prompt you to correct. One is not likely to recognize all categories automatically. However, a
system that helps you track better manually could be useful for improving data quality, and over time training
a more sophisticated automatic recognition system.

\subsection{Integrate with commitment services}
Tracking time should give a more accurate picture of where one is spending time. But better awareness might not be enough
to motivate behavior change in itself. For some people, negative reinforcement is a stronger motivator than positive reinforcement
\footnote{\url{https://blog.beeminder.com/punishment/}}. Beeminder is a web service that implements a system that can automate tracking progress towards
personal goals, using negative reinforcement in the form of taking one's money when one does not progress as planned.
In Beeminder, one sets a goal for how much/many/little of something one wants to do
on a daily/weekly/monthly basis. If one falls behind on the expected progress, the user's credit card is charged.
Beeminder is a tool to enable you to stick to behaviors you want to do that you know you can do but that
you still don't do.
The time tracking data collected in this system could be used as a powerful input to user-defined Beeminder goals.
Metabase has support for posting reports with data from dashboards \cite{metabase-api}.
Beeminder has an open API that accepts input from most sources \cite{beeminder-api}.
These two could be hooked up to allow attaching a dollar price via Beeminder to not meeting one's
goals of time spent doing x or doing less than x minutes of this per week/day.

Tools like Beeminder are tools that try to bind future you to a decision you know is the one you want
to make long term but that you are not likely to make in the moment without a powerful incentive. Attaching an automatic
payment and an accompanying reminder system can lead you to progress towards personal goals more consistently.
The payment size increases exponentially each time you slip to stop you from just paying up again and again.

This sort of setup is certainly not for everyone. But the developed setup with Metabase as an aggregator should make for a
quite simple integration with the Beeminder API if one wants to implement this.
