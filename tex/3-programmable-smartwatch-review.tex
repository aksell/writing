%!TEX root = ../Thesis.tex
\chapter{Comparison of programmable smartwatches}\label{cha:review}
There are many comparisons or rankings of smartwatches from a consumer's perspective.
But there are not many reviews of smartwatches from an application development perspective. What
follows is a review of three popular programmable smartwatches that will cover:
\begin{itemize}
    \item Programming experience - What tools and framework exist for the watches. What can one expect when programming against the watch
    \item Hardware features - What sensors and interfaces are available on the watches
    \item Community and documentation - What is the state of the developer community and how is the documentation
\end{itemize}


The three watches are chosen based on the qualitative criteria that they seem like the most often mentioned
watches when one browses around a bit for programmable smartwatches. There are not many to choose from.
A second criterion is that the watches had to be robust and usable enough to serve as one's primary watch, which means
at least a couple of days of battery life. Splash proof. With basic watch functionality like showing the time, setting timers, etc.

\begin{figure}[ht]
    \centering
    \includegraphics[width=\textwidth]{img/watches-labeled}
    \caption{Watches side by side}
    \label{fig:watches-labeled}
\end{figure}


\begin{figure}[ht]
    \centering
    \includegraphics[width=\textwidth]{img/watches-menus}
    \caption{Main menus}
    \label{fig:watches-menus}
\end{figure}

This chapter is also serves as research for what programmable watch to commit to
for an implementation of a smartwatch time tracking app in the following chapters.

\section{Pine time}
The PineTime is a smartwatch developed by the open-source hardware company PINE64.
PINE64 devices have open-sourced hardware designs.
PINE64 has a community-based software development model, following PINE64's philosophy of
"PINE64 does hardware while the community does the software".
The microcontroller used in the watch is a Nordic NRF52.

\subsection{Watch hardware}
The watch looks and feels sleek.
The watch gets input from a single hardware button and a 240x240 pixel touch display.
The touch display is quite precise. Touch buttons that are 48x48 pixels (a 5x5 grid of buttons on the display) are useable, with few presses resulting in the the wrong registered input see figure \ref{fig:wasp-calc}.
\begin{figure}[ht]
    \centering
    \includegraphics[width=0.3\textwidth]{img/wasp-calc}
    \caption{48x48 pixel on display touch "buttons" in a Pine-time app}
    \label{fig:wasp-calc}
\end{figure}

The watches sensors are shown in table \ref{tab:pine-sensors}\cite{pineDocs}.
\begin{table}[ht]
    \centering
    \caption{Pine time sensors}
    \label{tab:pine-sensors}
    \begin{tabular}{ll}
        \hline
        Accelerometer     & yes \\
        Magnetometer      & yes \\
        Gyroscope         & no  \\
        GPS               & no  \\
        Heart rate sensor & yes \\
        Microphone        & no
    \end{tabular}
\end{table}
Battery life is dependent on the watch firmware one installs. But one can expect a couple of days to a short week of battery life.
Most of the large firmware projects have not put much effort into battery optimization, so this could improve.

\subsubsection{Dev kit}
The PineTime is cheap, a dev kit is 25\$\footnote{\url{https://pine64.com/product/pinetime-dev-kit/}}. The dev kit comes with the watch un-sealed
enabling access to the PCB. The watch is currently only sold sealed in packs of 3, as
the watch is still considered to be in development. And there are no guarantees for
the stability of over-the-air updates, because of this, there is a risk of bricking a sealed
device. One then has to destroy the waterproof seal to access the PCB for re-flashing the watch firmware.
\begin{figure}[ht]
    \centering
    \includegraphics[width=0.3\textwidth]{img/pine-devkit}
    \caption{Pine time devkit contents with an unsealed watch}
    \label{fig:pine-devkit}
\end{figure}

The PineTime ships flashed with the watch firmware called Infinitime \footnote{\url{https://github.com/JF002/Pinetime}}.
Infinitime is written in C++ and the main libraries it uses are FreeRTOS, LVGL(graphics library), and NimBLE(Bluetooth).

\subsection{Existing software}
There are multiple watch operating systems in different stages of development for the PineTime \footnote{\url{https://wiki.pine64.org/wiki/PineTime_Development}}. The most notable one's are:
\begin{itemize}
    \item \textbf{infinitime} - Written in modern C++ based on FreeRTOS \cite{infinitime}. Devkits ship with infinitime pre-installed
    \item \textbf{wasp-os} - Micropython based watch development environment \cite{wasp-os}. Has a REPL and simulator.
    \item \textbf{pinetime-rust-mynewt} - PineTime firmware based on rust and Apache Mynewt OS \cite{mynewt}.
\end{itemize}
There is support for flashing wasp OS over the air from a watch flashed with infinitime.
The wasp-os has the most well-developed developer tooling, with a working firmware simulator, a nice REPL for running code
on the watch, or updating parts of the watch's software.
Infinitime and pinetime-rust-mynewt both have interesting applications and are worth considering depending on
what language or embedded software framework one prefers working in. Because wasp-os has the most well-developed
developer tooling, it will be the focus for the developer experience part of the review.

\subsection{Developer experience}
As mentioned, new watches ship with infinitime installed. Infinitime comes with a bootloader that supports over-the-air updates.
Over-the-air updates is still not considered reliable, so one could have to re-flash watch firmware via pins on the watch PCB.
If one buys an older watch the bootloader might not support over the air updates, one can update this by re-flashing
the firmware by accessing the un-sealed device's pins.

\subsubsection{Over the air firmware updates}
Phone apps such as NRF connect makes over the air update of the watch firmware quite easy.
Both the infinitime and wasp-os GitHub repositories have automatic builds setup for each new software version. Build files can be downloaded in their
Github repositories.
A PineTime watch running infinitime can be flashed with wasp-os over the air,
this works both ways.

\subsubsection{Wasp-os apps}
Wasp-os apps are implemented as Python classes. The apps
have a well-established structure.

An application is expected to implement an \mintinline{python}{init} and \mintinline{python}{foreground} method,
as well as some other optional state transition methods.
App states with their corresponding transitions and implementable behaviors
are shown in figure \ref{fig:wasp-lifecycle} from the wasp docs \cite{wasp-lifecycle}.
\begin{figure}[htbp]
    \centering
    \includegraphics[width=0.5\textwidth]{img/wasp-lifecyle-diagram}
    \caption{Wasp-os app lifecycle, with function names an app implements for the state transitions}
    \label{fig:wasp-lifecycle}
\end{figure}

\subsubsection{Counter app}
A basic watch app that increments a counter when the left side of the screen is pressed
and decreases a counter when the right side is pressed looks like this.

\begin{minted}{python}
import wasp

class SwipeCounter():
    """Swipe up to increment, swipe down to decrement."""
    NAME = "Swipe count"
    # App state is declared in init
    def __init__(self, initial_count=0):
        self.count = initial_count
    # Called when app is opened
    def foreground(self):
        self._draw()
        wasp.system.request_event(wasp.EventMask.TOUCH)
    
    def touch(self, event):
        if(event[0] == wasp.EventType.UP):
            self.count += 1
        elif(event[0] == wasp.EventType.DOWN):
            self.count -= 1
        self._draw()

    def _draw(self):
        draw = wasp.watch.drawable
        draw.fill()
        draw.string(self.count, 0, 108, width=240)
\end{minted}
The application registers for touch events by requesting touch events once foregrounded
via \mintinline{python}{wasp.system.request_event(wasp.EventMask.TOUCH)}. The app is then
expected to implement a function accepting a touch event as input.

\subsubsection{Testing and uploading the app}
A natural first step to test an app is to test it on the simulator. This
will catch syntax errors, and allows testing the visuals and functionality of the app. The simulator
does not accurately model memory and the exact hardware, so one still needs
to test on actual hardware.

To upload to the simulator:
\begin{minted}{shell}
    ~ make sim
    >>> from swipecount import SwipeCount
    >>> wasp.system.register(MyApp()) # Uploads the app
    >>> wasp.system.run() # Starts wasp with our app loaded
\end{minted}

Bluetooth pairing is not implemented for
wasp-os, so having the PineTime close to one's computers is all that is required to upload an app over Bluetooth.
Uploading the app to a physical watch is a single command:
\begin{minted}{shell}
    wasptool --exec swipecount.py --eval "wasp.system.register(SwipeCount())"
\end{minted}

\subsection{Community and documentation}
\subsubsection{Pine time community}
The PineTime has an active software development community. The projects with the most focused development are wasp-os and infinitime, but several other firmware projects are also maintained.
There is an active PineTime section in Pine64's forum with 5-10 posts a month.
There are also active direct help and discussion channels that are fairly active on matrix and other chat platforms.

\subsubsection{wasp-os}
Wasp-os har excellent documentation where the setup of development environment, flashing, and application development is detailed \footnote{\url{https://wasp-os.readthedocs.io/en/latest/}}.
The reference documentation is readable, concise, and consistent.
\subsection{Phone sync}
A basic integration Gadgetbridge integration exists for wasp-os. The support for Gadgetbridge is as of now
quite limited, but the basic foundation is there for contributing further
functionality if one needs it.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Bangle.js}
The Bangle.js is an off-the-shelf affordable smartwatch flashed with a slightly
customized Espruino javascript interpreter. Similar to the
PineTime, the Bangle.js uses a nordic NRF52 microcontroller. It comes flashed with an app loader and some stock apps.

\subsection{Watch hardware}
The watch has three physical buttons and a two-zone touch display, which can be used as touch buttons
or for horizontal swipe gestures.
Most in-app navigation uses the watch's three hardware buttons rather than the two-zone touch display.
The watch's stand by battery time is one week.
The Bangle.js is by far the largest and bulkiest of the watches. The Bangle.js looks and feels like a large diving watch.

The watch is not glued shut. It uses screws and a rubber seal. This means the watch can be opened without
breaking the waterproof seal if one is unlucky and bricks the device. The watch has a GPS and Glonass receiver.
For the full watch spec see \cite{bangle-specs}.
\begin{table}[ht]
    \centering
    \caption{Bangle.js sensors}
    \label{tab:bangle-sensors}
    \begin{tabular}{ll}
        \hline
        Accelerometer     & yes \\
        Magnetometer      & yes \\
        Gyroscope         & no  \\
        GPS               & yes \\
        Heart rate sensor & yes \\
        Microphone        & no
    \end{tabular}
\end{table}
The Bangle.js is the priciest of the watches and costs around 95\$.

\subsection{Existing software}
The Bangle.js has a free "app store"\footnote{\url{https://banglejs.com/apps/}} where one can upload and
update apps from an open-source web-based app store using web Bluetooth.
The Bangle.js has an active development community with over 200 open-source apps.
\begin{figure}[ht]
    \centering
    \includegraphics[width=0.5\textwidth]{img/bangle-apploader}
    \caption{Bangle app loader}
    \label{fig:bangle-apploader}
\end{figure}

\subsection{Developer experience}
With well-developed custom tools for Bangle.js app development, the Bangle.js has a very low barrier to entry
for app development. There are well documented and simple API functions for graphics and the watches other
hardware functions\footnote{\url{https://www.espruino.com/Reference\#Bangle}}.
The Bangle.js development experience feels unusually polished for someone used to more typical embedded development platforms.

\subsubsection{Web IDE}
The Espruino web IDE uses web Bluetooth to connect to a Bangle.js watch from a browser. Being web-based there is
nothing to install to get started developing apps for the Bangle.js.
The IDE features a code editor and a REPL that can be used to interactively run commands or inspect variables in running applications.
The IDE supports uploading files to the RAM or to the flash file system, as well as altering and reading all files on the
watch from the editor.
\begin{figure}[ht]
    \centering
    \includegraphics[width=0.8\textwidth]{img/espruino-ide}
    \caption{Espruino web IDE connected to a Bangle.js, left side is the REPL, right side is the editor}
    \label{fig:espruino-ide}
\end{figure}

\subsubsection{Counter app}
A basic counter app using the up and down button to modify a count
looks like this.

\begin{minted}{javascript}
function drawCount(count){
  g.clear();
  g.drawString(count, 100,100);
}
let count = 0;
g.setFontVector(56); // Font size
drawCount(count); // Initial draw
// Watch for buttons clicks, calls the provided function on clicks
setWatch(() => {count++; drawCount(count);}, BTN1, { repeat: true });
setWatch(() => {count--; drawCount(count);}, BTN3, { repeat: true });
\end{minted}
When an app is opened, the script will be run once. Events for buttons
or timers for redraws can be configured to drive the app's UI or functionality.

\subsubsection{Testing and uploading watch apps}
The app can first be tested on the visual simulator in the Espruino IDE, without any hardware. This catches syntax errors, and one can verify the app's visuals and basic function.
After verifying in the simulator, one can pair the Bangle.js with the IDE and upload the
app to RAM. The app can be removed by resetting the watch.
Once one is happy with the app, the app can be added as a persistent app in the Bangle.js's app launcher.
To do this one uploads the same file in the IDE but chooses to store it in a persistent file in flash.
To register the app in the app launcher, one simply uploads an appname.info file to flash memory.

\begin{minted}{javascript}
require("Storage").write("counter.info",{
  "name":"Counter",
  "src":"counter.app.js"
});
\end{minted}

\subsection{Community and documentation}
This is where the Bangle.js and Espruino combo shines.
The Espruino reference documentation is great. The community is large and active with many existing apps.

\subsection{Phone sync}
The Bangle.js has well-developed Gadgetbridge support\footnote{\url{https://www.espruino.com/Gadgetbridge}}.
The Bangle.js supports paring with Gadgetbridge and other useful features like:
\begin{enumerate}
    \itemsep0em
    \item Phone notifications
    \item Reporting activity data to Gadgetbridge. Heart rate and steps.
    \item Weather info
    \item Music playback controls
    \item Accepting calls
\end{enumerate}
There is support for sending messages both ways from Gadgetbridge to the Bangle.js.

Sending info to the phone from the Bangle.js is done via the Bluetooth API.
\begin{minted}{text}
Bluetooth.println(JSON.stringify({t:"info", msg:"Hello World"}))
\end{minted}

Sending messages to a Bangle.js can be done directly from the Espruion REPL:
\begin{minted}{text}
GB({"t":"musicstate","state":"play","position":0,"shuffle":1,"repeat":1})
\end{minted}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Pebble Time}
The \textit{Pebble Time} is a smartwatch produced by a now-dissolved company, but with a still active
development community.
The pebble has a unique screen with an e-paper like display that is visible even when the screen's backlight is off. This makes it feel more
like an analog watch, and the watch display is easily visible in bright sunlight.
The pebble does not have a touch display and relies solely on 4 buttons for input.

The watch firmware is not open-source. It is programmable thanks to a well documented C API that can be used when writing
apps.
There is also support for writing and running javascript applications that run on a phone
but display data on the watch.

The pebble watches are no longer in production, and as such, they have to be bought used. Used prices typically start at around 50\$.
The watch still has a fairly active following with active open-source development. The Rebble alliance spearheads the development of the larger projects in the community. The Rebble team aims to
maintain and develop the key features of the pebble watches in the absence of the pebble company.
That means maintaining the app store, the supporting web services used by the watch, and potentially revamping the watch firmware
and companion apps further down the line \cite{rebble-faq}.


\subsection{Watch hardware}
A pebble time is a slim and good-looking watch that feels much like a regular digital watch.
The e-paper display makes the watch feel more like a normal watch as it does not require button pushes
or arm movement to turn on the display. It is always ready to be read.
One button on the left side and three buttons on the right side are used for navigating menus.

The pebble time has a roughly one week long battery life. As the watches are getting older,
due to only used watches circulating the average battery life of a watch will vary depending on its
previous use. It is possible to order and replace the pebbles battery, but it is a fiddly process that
requires some special tools and one will break the watches water intrusion seal \cite{pebble-battery}.
\begin{table}[ht]
    \centering
    \caption{Pebble time sensors}
    \label{tab:pebble-sensors}
    \begin{tabular}{ll}
        \hline
        Accelerometer     & yes \\
        Magnetometer      & yes \\
        Gyroscope         & no  \\
        GPS               & no  \\
        Heart rate sensor & no  \\
        Microphone        & yes
    \end{tabular}
\end{table}

\subsection{Existing software}
The watch firmware is not open-source. There is a well-documented  C and javascript API
one uses to build apps for the watch.

The pebble app store has the widest selection of watch apps of the reviewed smartwatches.
Some of the apps in the app store are open-sourced.

\subsection{Developer experience}
Of the three watches, the pebble has the most well-developed watch app library. This means many programming challenges are solved, and one can rely on APIs being reasonably stable. The wide array of existing apps also provides helpful example implementation for varied and complex uses of the watche's APIs. The apps are aging, and most are no longer maintained, so the usefulness of this
will likely decrease somewhat.

There is a good selection of tutorials and guides on the rebble documentations page with examples for how
to persist data, use the dictation functionality, using the graphics functions, and more \cite{pebble-guides}.

\subsection{Community and documentation}
There are good guides for creating many types of watch apps hosted on the rebble site, as well as good documentation
of the C and javascript APIs \cite{pebble-api}.

There is an active community of users on Reddit with daily posts. The pebble watches still have a sizable active community.

\subsection{Phone sync}
The pebble time has well-developed Gadgetbridge support. There is also a proprietary app with sports and health
data tracking. The proprietary app can also be used to download apps.


\section{Comparing the smartwatches}

Summarizing the watches' relative strengths and weaknesses, we have:
\begin{itemize}
    \itemsep0em
    \item \textbf{Pine time + infinitime} Do you like writing C and have experience with freeRTOS? Do you have the patience to build a C project? Then the PineTime with the infinitime firmware is likely a good fit.
    \item \textbf{Pine time + wasp-os} Do you want a sleek watch. But you are not interested in building a C project each time you want to update an app on your smartwatch. Then PineTime with wasp-os is a good fit. Micropython programming with a decent simulator. Upload code easily and evaluate code on the watch using a simple command-line REPL. Wasp-os has great documentation and tidy code.
    \item \textbf{Bangle.js} If looks are not a problem, or you like the big clunky watch aesthetic. If you want a polished developer experience with a low barrier to entry and are comfortable writing javascript, the Bangle.js is a good fit.
          The Bangle.js has a feature-rich web IDE, with a simulator, REPL, and a user-friendly on-watch file system. A self hostable app store
          allows downloading and configuring apps from a phone or laptop via a browser.
          If you are interested in easy upload or download of data from and to the watch, the Bangle.js is the strongest candidate, as the app loader can be extended with custom web apps for building data interfaces with the watch.
          If you need GPS support, the Bangle.js is a clear choice because it supports multiple positioning systems.
    \item \textbf{Pebble time} You want a useful and sleek smartwatch that can be programmed. You are okay with buying a used watch. You prefer a more polished watch software feel with great aesthetics. The pebble time is the only watch with a microphone and speech-to-text support if this is a requirement.
\end{itemize}

\subsection{Time tracking app}
The goal of this review was to investigate affordable and programmable smartwatches to find a suitable
smartwatch to build a time tracking app for.

The key smartwatch requirements for building a time tracking app are:
\begin{enumerate}
    \itemsep0em
    \item Speed of development: Having more of the legwork done reduces the time needed before one can start making useful software.
    \item Active community that is likely to stick around, so that one can rely on the hardware being available for a long time, reducing the risk of having to port the application to another platform because the watch is no longer actively developed.
    \item The watch should look and feel all right, such that one is likely to want to use it.
\end{enumerate}

Evaluating the three smartwatches with these criteria in mind, we have that:
The pebble time has a nice appereance. The application development guides and APIs look well made and easy to follow. Nevertheless, it is a watch that is no longer in
production, making it a weak candidate for building an accessible app with an extended lifespan. Because of this, the pebble time is not the right watch for this project.
The PineTime is a good-looking watch.
The choice to not use the PineTime comes down to the two software/OS projects for the PineTime that were considered.
Infinitime, the core watch software project, uses C++ and freeRTOS, with few developer tools built to speed up app development.
Wasp-os has many nice tools, such as the REPL and simulator, but it does not have a finished
Gadgetbridge integration. So one would have to do most of the work with developing the phone integration for Gadgetbridge
before building an application that relies on syncing data with a phone.
This makes the Bangle.js the right choice for this project. The Bangle.js has a wide variety of applications developed for it with a
user-friendly app store that makes downloading and configuring applications via a phone or laptop easy.

The Bangle.js is the strongest candidate because of its great developer tooling, large community, and well developed
Gadgetbridge support.

The Bangle.js is a bit clunky and has the worst physical appearance. But the other factors are what tip the scale
in the Bangle.js's favor. There is also talk of a smaller Bangle.js. The time tracking application built for the Bangle.js
could probably be ported to the new smaller Bangle.js with little hassle, so the current physical appearance does not
get great weight in the evaluation.