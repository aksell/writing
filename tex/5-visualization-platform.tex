%!TEX root = ../Thesis.tex
\chapter{Data visualization platform}\label{cha:visualization}
Gathering data about how one spends time is only useful if it can be used to understand behavior or motivate
positive behavior change. Gadgetbridge does not present time tracking data in the app, and the watch app
is too limited to do longer-term graphing or analysis of the gathered data.
A visualization and analysis tool is needed for unlocking the value in the tracked data.
This chapter will discuss and detail a proof of concept implementation for a visualization system with a data pipeline out of Gadgetbridge.

\section{Requirements}
Visualizations of time tracking data from Gadgetbridge should be easily viewable at home on a laptop
or from a phone app.

It should preferably be able to use the Gadgetbridge SQLite database directly. Reusing the Gadgetbridge database would remove the
need to maintain a second copy of the time tracking data outside of Gadgetbridge. This would add complexity, but it is not a hard requirement.
\begin{itemize}
    \item Supports an SQLite database for input, as this is the only exportable format from Gadgetbridge.
    \item Possible to self-host for privacy and data ownership.
    \item Open source so that it can be extended and customized.
    \item Flexible graphing options to display data in a motivating and engaging dashboard.
    \item Supports live updates as new data is added. A lag of a few hours from real-time is acceptable.
\end{itemize}

\section{Design}\label{sec:visualization-design}
Adapting or reusing an existing solution for visualizing time interval and time-series data
is preferable, as it reduces development time, it is likely well tested, and one can tag along for future feature updates.

Three paths were considered for the data visualization.
\begin{enumerate}
    \item \textbf{Add visualizations to Gadgetbridge app}:
          Nice because the data is already in Gadgetbridge's database. And it would not require additional setup for users.
    \item \textbf{Push data to existing time tracking tools}:
          There are many existing time tracking tools,
          both paid, free, and free open-source tools exist. See section \ref{sec:manual-time-tracking} for an overview. These tools have nice visualizations and sometimes analysis of time tracking data one can use. For these to be useable with smartwatch data, they must have an open API that allows pushing time tracking data from an external source.
    \item \textbf{Use a flexible graphing  and data dashboard tool}:
          Nice because one will be free to set up graphs and dashboards customized to one's individual preference.
          This would require more setup from the user.
\end{enumerate}
The three options are discussed in the following sub-sections.

\subsection{Visualizations in gadgetbridge}
Gadgetbridge could, in theory, be extended with visualizations of time tracking data.
This is problematic because Gadgetbridge tries to be a bridge for exporting data from smartwatches and smart bands. Because of this, there is little emphasis put on views for visualizing information.
There are some visualizations in the app, but they are for common smartwatch features, such as sleep quality, active time, and steps.
Time tracking is not a standard function for smartwatches. Therefore it would likely be a tough sell to integrate flexible plotting
of time tracking data in the main Gadgetbridge Github fork. One would also have to build all plotting tools for time tracking data from scratch
as flexible graphing of data with a format defined outside of Gadgetbridge is not currently done.

\subsection{Push data to an existing time tracking tool}
Data from Gadgetbridge could be pushed to a time tracking tools database.
This could be implemented by doing periodic full
imports of the SQLite database from Gadgetbridge if the tool supports importing SQLite databases.
Alternatively, one could build a program that reads the database export
from Gadgetbridge, looks for new entries, and pushes them to the time tracking services API.
The integration with the time tracking tools API could not be done from within Gadgetbridge, as Gadgetbridge has a strict
policy to not ask for internet access permissions \cite{gb-cloud-sync}, to ensure data privacy. Because of this policy, Gadgetbridge is unable to communicate with external APIs.
If one implemented a middle layer that read the Gadgetbridge database exports, looked for new entries, and pushed them
to a time tracking tool, the watch app and Gadgetbridge setup would function as an easy to use and fast input device for the
service.
Using an existing service would allow one to use the time tracking tool alongside
the watch app. One could, for example, use the time tracking tool directly when at one's desk and the watch app when not at a desk.
Time tracking software is often built primarily to simplify tracking time at work and billable hours \cite{wiki-time-tracking}.
With limited flexibility in custom visualizations or goal setting and visualizations geared towards visualizing work time, not personal time tracking data.
A solution like this would require one to give up time tracking data to the company hosting the time tracking service. This breaks with the requirement for self-host ability and personal ownership of the data. This in addition to the fact that a middle layer that handles pushing changes to the exported database from
Gadgetbridge would have to be implemented, makes this path unfeasible.


A twist on this approach is using an existing self-hosted and open-source time tracker.
There are existing open-source and potentially self-hostable time tracking solutions. The relevant ones are Kimai and hamster\footnote{\url{https://github.com/projecthamster/hamster}}.
Kimai is primarily aimed at tracking billable hours, and the graphing
tools are relatively simple. Hamster is unique in that it is intended for tracking personal time, not billable hours.
"Hamster is time tracking for individuals. It helps you to keep track of how much time you have spent during the day on
activities you choose to track."
Hamster has very basic visualizations and has had problems with active maintenance. The graphing options are limited, and
and it lacks documentation.
It is a tool primarily for fast capture of time tracking data, not flexible graphing and analysis \cite{hamster-is}.
Hamster is, therefore, not the right tool either.

\subsection{Flexible data dashboard and analytics tools}
We are left with option three with the two other options out of the game: using an existing flexible graphing and data dashboard tool.

To minimize complexity when implementing a visualization tool outside of Gadgetbridge, we opt for keeping Gadgetbridge the
data master. With this approach, data will not be changeable in the visualization tool, and data changes are
not expected to propagate back to Gadgetbridge. Having one-way sync of data out of Gadgetbridge reduces the complexity
by not needing to keep two databases in sync with changes propagating both ways, a notoriously tricky problem.
Keeping the full-time tracking data set in Gadgetbridge on the phone is not problematic storage-wise, as the time tracking
data generated is not very data-intensive. It is unlikely that the time tracking database will grow past a few megabytes evens
after prolonged use.

With Gadgetbridge as the data master, the exact tool one uses for visualizing the data is up to the
user, as the only layer between the visualization tool, is a transport layer that syncs the exported data from
Gadgetbridge to the visualization tool.

There are several interesting open-source, self-hostable tools for this approach:
\begin{enumerate}
    \itemsep0em
    \item \textbf{Superset}\footnote{\url{https://github.com/apache/superset}} - Self-hostable, open-source, with a large, and customizable set of visualizations
    \item \textbf{Redash}\footnote{\url{https://github.com/getredash/redash}} - Self-hostable, open-source, and easy to use.
    \item \textbf{Metabase}\footnote{\url{https://github.com/metabase/metabase}} - Self-hostable, open-source, and easy to use.
\end{enumerate}

All of these could likely be a good fit for this project. All the tools are open source and self hostable, with
support for importing SQLite databases.
Metabase and Redash are the ones with the larges focus on ease of use, whereas Superset has a focus on more complex visualizations,
performance, and customization through plugins.
Metabase is chosen as the visualization tool for this chapter because of its easy-to-use graphing tools and easy setup.
The graphing tools provided by Metabase will likely be feature-full enough for most users.
If a user wants to set up either Redash or Superset, the setup described in this chapter can largely be reused as the three tools
function similarly.

A downside of using a general data visualization and dashboard tool instead of a tool specialized for visualizing
time tracking data is that more setup is needed from the users to create their own visualizations.
For this higher initial setup effort, one gets great flexibility and options for customizing visualizations
to one's needs.

\subsection{Exporting time tracking data}
Metabase is not a phone application, so the data must be synced to a Metabase instance running somewhere.
The test instance will run on the author's laptop as the author is the only person who will access
the data. If one is motivated by transparency and accountability, one could run Metabase in a
cloud hosting service on a public domain. Metabase has an authentication and user system that allows others to see one's plotted time tracking data if one chooses to give someone a user.

To set up visualizations in Metabase, we need a tool that syncs data from a local folder on the phone (as Gadgetbridge does not have
internet access, and is incapable of syncing data out of the phone) to a different device running Metabase.
Syncthing is the perfect tool for syncing data from the phone's Gadgetbridge database to another machine's Metabase
instance. Syncthing supports peer-to-peer syncing of folders between hosts.
This can be paired with Gadgetbridge's automatic database export feature for a sync solution that requires
no central cloud hosting. Enabling a private personal data pipeline for time tracking data.

\subsection{Data flow}
\begin{figure}[ht]
    \centering
    \includegraphics[width=1\textwidth]{img/data-flow}
    \caption{Data flow from Bangle.js application to Metabase}
    \label{fig:data-flow}
\end{figure}

The proposed data flow from Bangle.js via Gadgetbridge, through Syncthing, and into Metabase is shown in figure \ref{fig: dataflow}.
Syncthing needs to be set up on both the phone running Gadgetbridge and the computer hosting Metabase, the two devices
then have to set up a shared synced folder.

Metabase is run in a persistent docker container on the laptop that is started automatically on boot.
Metabase stores its database in volume on disk so that the app's state and user data persist after a shutdown.

This way, Gadgetbridge remains the data master.
Metabase is a pure visualization layer without data modifications.

\section{Implementation}
This section details how Syncthing and Gadgetbridge are set up to sync time tracking data to Metabase automatically. It also shows how one can create some simple time tracking data visualizations in Metabase.

\subsection{Data from Gadgetbridge to laptop}
The android device running Gadgetbridge must install the Syncthing app.
So must the machine hosting Metabase.
A shared folder is created, then the two devices must both be set up with the ID of the other,
so that a shared folder can be created.

A shared folder is created, then the phone and hosting machine are paired using Syncthings shareable keys.
Now a shared local folder on the android device and the hosting machine is set up.
Next, Gadgetbride needs is set up to export its time tracking database at a regular interval to
the local synced folder on the android device. This is supported in Gadgetbridge and enabled in the app's settings.

Syncthing is set to start on boot for a linux machine via a .desktop file that launches the app.
\begin{minted}{shell}
   cp /usr/share/applications/syncthing-start.desktop .config/autostart
\end{minted}

Now the Gadgetbridge database is available in the synced folder on the computer.

\subsubsection{Running Metabase}
Metabase is run via docker.
\begin{minted}{docker}
    docker run -d -p 3000:3000 \
        -v ~/metabase-data:/metabase-data \
        -v ~/Sync:/Sync \
        --restart unless-stopped \
        -e "MB_DB_FILE=/metabase-data/metabase.db" \
        --name metabase metabase/metabase
\end{minted}
Metabase stores info about dashboards that are set up, users created, and more in a database.
A docker volume is mapped from the user's home folder into the container
to have the Metabase database persist between reboots, \mintinline{shell}{-v ~/metabase-data:/metabase-data}.
The \mintinline{shell}{-e "MB_DB_FILE=/metabase-data/metabase.db"} option tells Metabase to
store its data in the Metabase data folder outside the docker container.

The folder synced from the phone running Gadgetbridge is mapped into the container to allow Metabase to access
Gadgetbridge's data by creating a docker volume \mintinline{shell}{-v ~/Sync:/Sync}.

The container is set to restart with \mintinline{shell}{--restart unless stopped}. This ensures the Metabase containers will
be restarted after a reboot or other events that would cause the container to die.

\subsection{Metabase dashboards}
Metabase now has access to the regularly synced time tracking data in a local folder inside the hosting docker container.
Metabase is set up with this local database as a data source. How to do this is well documented by Metabase\footnote{\url{https://www.metabase.com/docs/latest/administration-guide/01-managing-databases.html}}.
The Gadgetbridge database is now available for queries in Metabase.

Metabase has some helpful graphical tools for doing queries and making visualizations, and combining them in
visually appealing dashboards that update live. If one has some familiarity with SQL,
writing pure SQL is often simplest as soon as one moves beyond the most basic visualizations.

\begin{minted}{sql}
SELECT sum(STOP-START)/60 as "Minutes", 
CATEGORY_NAME, DATE(datetime(START, 'unixepoch', 'localtime')) as 'Day'
FROM TIME_TRACKING_SAMPLE
GROUP BY DATE(datetime(START, 'unixepoch', 'localtime')), CATEGORY_NAME
ORDER BY START desc
LIMIT 100;
\end{minted}
The SQL in the above graph generates the data for the plot shown in figure \ref{fig:metabase} that can be added to a dashboard.
There are easy-to-use tools for displaying the queried data in Metabase dashboards using live plots.

One could now open one's local Metabase instance to see one's time tracking data.
Suppose a more permanently viewable solution is desired. In that case, one could set up a dedicated monitor to always display one's time tracking dashboard
from Metabase somewhere at home or wherever access to time tracking data or other personal analytics could be useful.

\begin{figure}[ht]
    \centering
    \includegraphics[width=0.8\textwidth]{img/metabase-plot}
    \caption{Plot of time spent in each time tracking category per day, in a Metabase dashboard}
    \label{fig:metabase}
\end{figure}

\section{Evaluation \& discussion}
Automatic exports of the Gadgetbridge database and sync to a shared folder accessed by Metabase has proven to be
stable, flexible, and easy to set up. Metabase has been used as an example visualization service. It works well, and making visually appealing
dashboards with time tracking data is relatively simple.
Metabase is only one of several similar data visualization platforms. Three alternatives are mentioned in section
\ref{sec:visualization-design}. Using an exported database from Gadgetbridge directly in a visualization tool, the data storage
and modification is clearly separated from the visualization and analysis of the data. This makes plugging in one's preferred visualization
tool without dependence on a data export tool or processor possible. Metabase could easily be replaced by any other visualization
or analytics tool that can be self-hosted and supports imports of SQLite databases.
