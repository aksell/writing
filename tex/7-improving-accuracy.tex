%!TEX root = ../Thesis.tex
\chapter{Improving tracking accuracy}
Tracking time accurately can be hard.
A common source of errors is when one forgets to update the time trackings tool
active category when changing activities.

This chapter investigates increasing time tracking data quality by increasing time
tracking accuracy.

Approaches to improving tracking accuracy can be split into two categories:
\begin{enumerate}
    \itemsep0em
    \item Automate parts of the tracking process
    \item Make the user better at tracking time accurately
\end{enumerate}


This chapter discusses both approaches before implementing a solution for improving a user's manual tracking accuracy.

\section{Automating time tracking}
The name says it all. Can how one spends time be tracked by automatic systems that don't
rely on user input?
Time tracking systems for activities performed on digital devices already exist.
Tools like Rescuetime keep track of what applications one is actively using and what websites one is
spending time on. Tracking time automatically for activity on digital devices is already relatively well developed.
However, not all activities of interest are digital. To make a more complete automatic time tracking solution, one needs to capture activities with no digital signature.

Smartwatches and other wearables have various sensors, providing motion data, location data,
heart rhythm, and more. This data can be used to infer what activity a user is performing.
Automating tracking of nondigital activities via sensors on a smartwatch is widely researched.
A Google Scholar search for "smartwatch activity recognition" resulting in thousands of results\cite{scholarsmartwatch}.
Systems for detecting sitting, standing, different standing states, and the transitions between them
based on data from a single tri-axial accelerometer have been shown to work well \cite{Khan2010}.
Many smartwatches already try to detect and log a user's total "active time" in a day. Some smartwatches can detect
if users are standing or sitting.
More complex behavior have also been attempted recognized, such as drinking coffee, talking, and eating using a
hybrid smartphone and smartwatch model with motion data from both devices\cite{Shoaib2015}.
Despite the large interest and rapid development, it does not seem automatic activity recognition is at a
level where it could work as the only input to a time tracking system with user-defined categories. It would likely not be
able to tag activities based on sensor data in a way that proved useful without large amounts
of manual correction. Furthermore, the hardware
and software sophistication required would be way outside the scope of this thesis.


It could be that quite basic activity recognition could improve one's manual time tracking
accuracy. Instead of a fully automatic activity recognition system, the activity recognition
system could aid in manual tracking.

\subsection{Manual and automatic hybrid}
A requirement for the developed time tracking system was that time tracking categories are user-configurable. This means simple detection of body postures or activities like running or walking would not suffice.
To do activity recognition based on user-defined categories, the user would either have to provide
a unique activity profile or a set of activity characteristics that would distinguish tracking categories
from each other.
It is not clear that this would be doable in most cases, and for some sets of time tracking categories,
it would almost certainly be impossible. Consider trying to distinguish gardening from boat maintenance
or reading from watching a movie.

An alternative to the user having to characterize activities uniquely would be that a manual time tracking system was used to train an automatic time tracking system.
One would use the manual time tracking system like normal, but motion data could be captured and stored
alongside the time tracking data.

Over time this hybrid approach could allow building a more sophisticated and user personalized
activity recognition system, trained to recognize the user-defined set of time tracking categories
using the user's own motion data.

For a purely automatic activity detection system to be useful without any manual input, it would
have to be very accurate.
One could, as a first step, use activity recognition to improve the accuracy when tracking time manually,
by alerting the user when we think they are in the wrong category.
Such a system would try to recognize activities, comparing them to the expected activity profile of the
active time tracking category. If the activities do not match the expected one for a set amount of time,
the watch app could prompt the user to verify their current category. This way, the category recognition
model would, in practice, only have to recognize that one is not in one's current active category, with
reasonable certainty, instead of recognizing the exact activity the user is performing.

Other inputs than activity data could be helpful in estimating when a user is tracking time in the wrong category.
A model could be trained to recognize what an average entry for each time tracking category looks like.
A simple metric here could be entry length. How long is one's typical entry when in the category "reading"?
If it is rarely over an hour, the time tracking system could prompt the user if the open entry is more than three times as long as the average length to verify that one is still in the category and not tracking erroneously.


Most previous work on motion-based activity recognition uses accelerometer data from smartphones, like \cite{Zoltan2009}.
Smartwatches potentially have even richer motion data than smartphones because the device's body position is
known, and the smartwatch's position at the end of a limb would capture arm motions.

\section{Measuring tracking accuracy}
One way of improving time tracking accuracy is to help the user become better at manual time tracking.
Measuring and displaying one's time tracking accuracy could be a tool to help with this.
Time tracking accuracy can be measured by randomly prompting the user, asking them if they are currently
tracking time in the category that best reflects their actions in the exact moment of being asked.
If not, the user can correct their active category.
If these \textit{category checks} were very frequent, for example, on average 5 minutes apart, they would give a fairly accurate
picture of how the user is spending time. The user would not be in the wrong category for long because
the prompts would force the user to correct the active category.

This is close to what stochastic time tracking systems do. Except that existing system typically sacrifice accuracy
in the short term for less annoyance by sampling one's current activity much rarer than every 5 minutes (TagTime\footnote{\url{https://github.com/tagtime/TagTime}} uses a 45 minute
average interval between checks).
Stochastic time tracking systems do not have a concept of manually changing one's category. The only input used
is users answers to random checks asking the user to input what they are doing at this exact moment.
Data from these discrete samples could be used to measure one's manual
time tracking accuracy. Similarly to how stochastic time tracking systems try to "measure" how one spends time by gathering discrete
samples over time. One's time tracking accuracy would be high if one were consistently tracking time in the same category
as one input when prompted randomly to input what one is currently doing. One's accuracy would be low if one
often corrected to a different category because of the random prompts.

A simple implementation of this could be to add stochastic prompts asking the user what category they are in,
on average, once an hour. Then the data from these prompts could be stored as a measure of time tracking accuracy.
A mechanism like this is implemented in the following section.

\section{Implementing an accuracy metric}
A time tracking accuracy metric would enable the user to see how accurately they are tracking time.
It is easier to improve in something that is measured and clearly visible.
Measuring time tracking accuracy will also improve time tracking accuracy by having the user
correct their current category if "caught" in the wrong category by one of the random checks.
One's time tracking accuracy can presumably vary over time.

The system for measuring tracking accuracy will be implemented on top of the existing smartwatch based
time tracking system.
The accuracy metric is trying to determine how likely it is that the user is doing x if the user has currently selected
category x in the smartwatch time tracking app.

\subsection{Requirements}
The time tracking accuracy system should:
\begin{itemize}
    \itemsep0em
    \item Give random prompts on average 60 minutes apart, asking the user to verify their current tracking category.
    \item Store the category checks in the Gadgetbridge database.
    \item Answering a category check should lead to the current time tracking category being updated if the check "catches" the user in the wrong category.
    \item Allow configuring if one to track time tracking accuracy or not.
\end{itemize}

\subsection{Design}
A way to test if one is doing x when in category x in the watch app is to prompt the user with a message + vibration asking:
"Are you doing x?" \textit{yes} or \textit{no}. Answering \textit{yes} will register that, one was tracking time in category x.
Answering no opens a prompt allowing the user to correct their active category to the actual one.
This is stored as a category check, where the actual category is stored along with a timestamp.
The accuracy metric would be how high one's fraction of successful category checks to failed ones is. A successful check is one where the user was tracking time in the category that best reflects their current activity.

The prompt to verify one's category might not be answered. The user could ignore the prompt, or the watch might not currently
be worn. A deadline of 30 seconds to answer is chosen semi arbitrarily for this. If a check is not answered in 30 seconds,
it is stored as not answered.

\subsubsection{Prompt interval}
Random prompts can be disturbing, so there is a tradeoff between wanting not to disturb the user too often and
wanting a measure of accuracy that is accurate even for shorter time frames.

To avoid any biases in the measurement of tracking accuracy, the prompts should be random with a reasonable average
interval.

The existing stochastic time tracking application TagTime uses a 45-minute interval.
Because stochastic prompts are not the only source of time tracking data in the manual time tracking app developed in the previous sections,
we can allow more infrequent checks.
The default interval is set to an hour. This interval should preferably be user-configurable, as one can have different
interest in what time frame one wants to measure time tracking accuracy over.

\subsubsection{Category check model}
The relevant information to store from a category check is
\begin{enumerate}
    \itemsep0em
    \item Timestamp
    \item Active category - The active time tracking category at the time of the category check
    \item Actual category - The category one corrects to if one was in the wrong category, or the same as the active category if
          one is tracking correctly.
    \item Answered - Whether the category check was answered or not
    \item Average gap - The average interval in seconds between category checks.
\end{enumerate}

The average gap is the configured average time in seconds between category checks.
This is stored because the average interval could be changed. A category check must be seen relative
to its average gap if one is
to be able to compare category checks from periods where the average gap has been changed.
A category check from a period where the average gap was configured to be shorter than in
another period should result in giving the checks from the period where checks where
on average, more frequent less weight.

The resulting database model can be seen in \ref{fig:category-check-model}.

\begin{figure}[ht]
    \centering
    \includegraphics[width=0.6\textwidth]{img/category-check-db}
    \caption{Gadgetbridge database models for category checks}
    \label{fig:category-check-model}
\end{figure}


\subsection{Implementation}
A periodic call to a function checking if a category check is due by comparing a check threshold to the
built in random function is added to the watch app.
\begin{minted}{javascript}
// Check if a category check should trigger once a min
const CHECK_INTERVAL = 60 * 1000; 
// Average of 60 min between category checks
const AVG_GAP_MS = 60 * 60 * 1000; 
// Probability of a check in an inverval
const CHECK_PROB_IN_INTERVAL = 1 - CHECK_INTERVAL / AVG_GAP_MS; 
const CHECK_ANSWER_IN = 30 * 1000; // 30 sec to respond to prompt

// Once every CHECK_INTERVAL milliseconds a test against random is performed
showCategoryCheck = Math.random() < CHECK_PROB_IN_INTERVAL;
if(showCategoryCheck){
    // Perform a category check, prompting the user to verify their category
}
\end{minted}

\begin{figure}[htbp]
    \centering
    \begin{minipage}{.45\textwidth}
        \centering
        \includegraphics[width=0.9\textwidth]{img/category-check-prompt}
    \end{minipage}%
    \begin{minipage}{.45\textwidth}
        \centering
        \includegraphics[width=0.9\textwidth]{img/category-check-select-correct}
    \end{minipage}
    \caption{Category check prompt to check the current time tracking category, and the view if one
        answers no and one has to correct one's category}
    \label{fig:category-check-prompt}
\end{figure}

Figure \ref{fig:category-check-prompt} shows how the category check prompt is implemented.
The watch vibrates to alert the user once the prompt opens.
If one answers yes, the category check is stored with the actual activity field equal to the active
category.
If one answers no, one is prompted to correct one's category. Once one corrects the category
the check is stored with the updated actual category.
If the check prompt is not answered in 30 seconds, the check is stored as not answered.

Category check data is stored locally on the Bangle.js in a CSV file and sent to Gadgetbridge in the same way
as time tracking entries. See section \ref{sec:send-to-gadgetbridge} for reference.
The time tracking accuracy data is then available in Metabase for use in visualizations alongside time tracking
data or in a separate pane dedicated to visualizing tracking accuracy.
The accuracy metric can also be broken down on a category or day level to visualize how one's accuracy varies
between categories or with other metrics.

\section{Evaluation}

\begin{table}
    \label{tab:checks}
    \centering
    \caption{Black box test of requirements}
    \begin{tabular}{p{0.82\linewidth} p{0.15\linewidth}}
        \toprule
        \textbf{Description}                                                                                                                              & \textbf{Completed} \\
        \toprule
        Give random prompts on average 60 minutes apart, asking the user to verify their current tracking category                                        & yes                \\
        \hline
        Store the category checks in the Gadgetbridge database.                                                                                           & yes                \\
        \hline
        Answering a category check should lead to the current time tracking category being updated if the check "catches" the user in the wrong category. & yes                \\
        \hline
        Allow configuring if one to track time tracking accuracy or not.                                                                                  & no                 \\
        \hline
    \end{tabular}
\end{table}

The implemented solution satisfies most of the functional requirements. The user customization requirement is not fulfilled; the random prompts cannot be disabled.

\section{Discussion}
The implemented system uses a static interval between category checks.
If a user is consistent in tracking time accurately and thus always answers the category checks successfully, there is little
reason to bother the user with periodic prompts.
Consistent, accurate tracking could lead to a backoff in the category check frequency. And similarly, a series of failed
prompts could result in decreasing the average check interval.

As the current category during the check is stored, one can have separate average prompt intervals for each time tracking category.
Maybe one is much more likely to update one's category when going for a run or when going from watching tv to cooking.
This could result in higher category check frequencies for the categories where one is likely to track time inaccurately.

Dynamic category check intervals have an interesting incentive structure.
One is "rewarded" for tracking time correctly by being disturbed less often by the category check algorithm.
Over time, this could lead to improved time tracking accuracy by rewarding accurate tracking with fewer category checks performed.

Dynamic category check intervals could be an interesting topic to explore further.
